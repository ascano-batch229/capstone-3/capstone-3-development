import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Modal, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function ProductView (){

	const navigate = useNavigate();

	const { user } = useContext(UserContext);
	const [productName, setProductname] = useState('');
	const [description, setDescription] = useState('');
	const [availableInStock, setAvailableInStock] = useState('')
	const [quantity, setQuantity] = useState('')
	const [orders, setOrders] =useState ('')
	const [price, setPrice] = useState(0);

	const { productId } = useParams();

	useEffect(() => {

		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setProductname(data.productName);
				setDescription(data.description);
				setAvailableInStock(data.availableInStock);		
				setPrice(data.price);
				setOrders(data.orders)

			});

	},[])

	const BuyNew = (productId) => {


	fetch(`${ process.env.REACT_APP_API_URL }/users/newOrder`, {

			method: "POST",
			headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
			productId: productId,
			quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Check out success!!",
					icon: 'success',
					text: "You have successfully purchased this products."
				})
				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" or "Navigate" component
				navigate("/products");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})

			}

		});

	}

	return (

	<Container className="mt-5 ">
			<Row>
				<Col lg={{ span: 12, offset: 0 }}>

					<Card className="" style={{height:'500px'}}>

						<Container>
							<Row>

								<Col md={4} className=" text-center mt-4" style={{height:'400px'}}>  
								<Card.Img variant="top" src="https://psediting.websites.co.in/obaju-turquoise/img/product-placeholder.png" style={{height:'400px'}}/>




								</Col>



								<Col md={8}>

											<Card.Body className="bg-light border border-round mt-4" style={{height:'400px'}} >

												<Card.Title> <h3>{productName}</h3></Card.Title>
												<Card.Subtitle>Description:</Card.Subtitle>
												<Card.Text>{description}</Card.Text>

												<Form.Label>Quantity</Form.Label>
					      						<Form.Control style={{width: "70px"}}type="number" placeholder="Enter Quantity" value={quantity}  onChange={(e) => setQuantity(e.target.value)} />

												<Card.Subtitle className="mt-2">Price:</Card.Subtitle>
												<Card.Text className="my-2">$ {price}</Card.Text>
												<Card.Subtitle></Card.Subtitle>
												<Card.Text></Card.Text>
												{
													
													(user.id !== null) ?
													<Button onClick={() => BuyNew(productId)}variant="primary" block> CHECK OUT </Button>
													
													:
													<Link className="btn btn-danger btn-block" to="/login"> Login </Link>
												
												}
												<Button variant="primary" className="m-2"> ADD TO CART </Button>
												

											</Card.Body>
								</Col>
							</Row>		
						</Container>
					</Card>

				</Col>
			</Row>
		</Container>



		)
}