// import Courses from './pages/courses.js';
import {useState, useEffect, useContext} from 'react';
import { Container, Card, Button, Row, Col, Modal } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';


export default function CourseView(){

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrieve the courseId passed via URL

	const { courseId } = useParams();

	const [productName, setProductname] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	 const [show, setShow] = useState(false);


	useEffect(() => {

		console.log(courseId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${courseId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setProductname(data.productName);
				setDescription(data.description);
				setPrice(data.price);

			});

	},[])

	const enroll = (courseId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/users/enroll`, {
			method: "POST",
			headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
			courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: 'success',
					text: "You have successfully enrolled for this course."
				})
				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" or "Navigate" component
				navigate("/courses");

			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})

			}

		});

	}



	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>

					<Card>

						<Card.Body className="text-center">
							<Card.Title>{productName}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle></Card.Subtitle>
							<Card.Text></Card.Text>
							{
								(user.id !== null) ?
								<Button onClick={() => enroll(courseId)}variant="primary" block> CHECK OUT </Button>
								:
								<Link className="btn btn-danger btn-block" to="/login"> Login </Link>
							}
							
						</Card.Body>		

					</Card>

				</Col>
			</Row>
		</Container>


		)
	}

