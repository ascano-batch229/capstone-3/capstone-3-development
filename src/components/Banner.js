// Destructuring components for cleaner codebase
import {Carousel} from 'react-bootstrap';


export default function	Banner(){
	return(
	    <Carousel>
	      <Carousel.Item interval={1000}>
	        <img
	          className="d-block w-100"
	          style={{height:'600px'}}
	          src="https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/168082855/original/f916460e640d6aef85cd25fa989ee502044331c7/design-banner-for-christmas-new-year-or-any-ecommerce-website-0c43.jpg"
	          alt="First slide"
	        />
	        <Carousel.Caption>
	          <h1>First slide label</h1>
	          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item interval={500}>
	        <img
	          className="d-block w-100"
	          style={{height:'600px'}}
	          src="https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/125457984/original/358e17239224e56a17586a918d7a3089d963e1c9/do-web-banner-design.jpg"
	          alt="Second slide"
	        />
	        <Carousel.Caption>
	          <h3>Second slide label</h3>
	          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-block w-100"
	          style={{height:'600px'}}
	          src="https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/278516101/original/81565224df8113a14f8d93de374a246d1be687a1/design-a-fantastic-web-banner-cover-ads-and-hero-image.jpg"
	          alt="Third slide"
	        />
	        <Carousel.Caption>
	          <h3>Third slide label</h3>
	          <p>
	            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
	          </p>
	        </Carousel.Caption>
	      </Carousel.Item>
	    </Carousel>
		)
}