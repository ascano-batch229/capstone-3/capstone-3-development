import {Row, Col, Card,Carousel} from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function Highlights({productProps}){

  console.log(productProps)
  console.log(typeof productProps)

  const {description, price, _id, productName, availableInStock} = productProps;


	return(

    <Col xs={12} sm={6} md={4} lg={3} className="mb-3">
          <Card style={{ width: '18rem', height: '27rem' }} className="fixed-height">
            <Card.Img variant="top" src="https://psediting.websites.co.in/obaju-turquoise/img/product-placeholder.png" />

            <Card.Body>
              <Card.Title>{productName}</Card.Title>


        <Card.Text>
                $ {price}
              </Card.Text>

              <Link className="btn btn-primary" to={`/ProductView/${_id}`}> Details </Link>
            </Card.Body>
          </Card>
    </Col>



		)

}