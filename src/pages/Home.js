import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'
import CourseCard from '../components/CourseCard.js'
import {Card, Button, Col, Row, Container} from 'react-bootstrap'
import {useEffect, useState} from 'react';


export default function Home(){

	const [products, setProducts] = useState([])

		useEffect(() => {

			fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        		.then(res => res.json())
        		.then(data => {

  				console.log(data)

  				setProducts(data.map(product => {
  					return (

  						<Highlights key={product._id} productProps = {product}/>
  						)
  				}))
    			})	
		},[])



	return(

		<>
			<Container>
    			<Banner/>
    		</Container>

    			<Container className="my-3">
    				<Row>
    				{products}
    				</Row>
				</Container>
    		
    	</>
		)
}