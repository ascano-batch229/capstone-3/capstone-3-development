import ProductCard from '../components/ProductCard.js'
import '../App.css';

import {useEffect, useState} from 'react';
import {Card, Button, Col, Row, Container} from 'react-bootstrap'

export default function Products(){

	const [products, setProducts] = useState([])

		useEffect(() => {

			fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        		.then(res => res.json())
        		.then(data => {

  				console.log(data)

  				setProducts(data.map(product => {
  					return (

  						<ProductCard key={product._id} productProps = {product}/>
  						)
  				}))
    			})	
		},[])


		return(

			<>
				<div className="d-flex justify-content-center m-3 prod-page-head">
					<h2> PPRODUCT PAGE </h2>
				</div>
			
			     <Container>

				    <Row>

				        {products}
				        	
				        </Row>

			    </Container>						
			</>




			)

}