import CreateProduct from '../components/CreateProduct.js'
import {Container, Row, Col} from 'react-bootstrap'
import DashNavBar from '../components/DashNavBar.js'
import ProductCatalog from '../components/ProductCatalog.js';
import {useState, useEffect} from	'react';

export default function Dashboard() {

	const [currentComponent, setCurrentComponent] = useState('ProductCatalog')

	useEffect(() => {
	console.log('currentComponent changed to:', currentComponent)
	}, [currentComponent])


	return(

		<>
			<Container>
				<Row>
					<Col md={4}>
						<DashNavBar setCurrentComponent={setCurrentComponent}/>
					</Col>
					<Col md={8}>
						{/*{currentComponent === 'CreateProduct' && <CreateProduct/>}*/}
						{currentComponent === 'ProductCatalog' && <ProductCatalog/>}
						
					</Col>
				</Row>
			</Container>
		</>


		)

}