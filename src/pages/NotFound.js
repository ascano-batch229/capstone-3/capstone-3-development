import { Link } from 'react-router-dom'


export default function	NotFound(){
	return(
			<>
			<h1> Error 404 </h1>
				<p> 
					Page Not Found. Go back to <Link to="/"> Home Page </Link> 
				</p>
			</>
		)
}